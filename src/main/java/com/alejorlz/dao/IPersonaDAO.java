package com.alejorlz.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alejorlz.model.Persona;

@Repository
public interface IPersonaDAO extends JpaRepository<Persona, Integer> {

}
