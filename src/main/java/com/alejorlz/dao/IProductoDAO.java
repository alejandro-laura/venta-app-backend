package com.alejorlz.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alejorlz.model.Producto;

public interface IProductoDAO  extends JpaRepository<Producto, Integer> {

}
