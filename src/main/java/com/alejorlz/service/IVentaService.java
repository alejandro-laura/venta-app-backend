package com.alejorlz.service;

import java.util.List;

import com.alejorlz.model.Venta;

public interface IVentaService {

	Venta registrar(Venta venta);

	void modificar(Venta venta);

	void eliminar(int idVenta);

	Venta listarId(int idVenta);

	List<Venta> listar();
}
