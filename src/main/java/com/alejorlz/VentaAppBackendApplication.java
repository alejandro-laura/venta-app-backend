package com.alejorlz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VentaAppBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(VentaAppBackendApplication.class, args);
	}
}
